package org.launchcode.training.carpersistence.controllers;

import org.launchcode.training.carpersistence.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CarController {

    @Autowired
    CarRepository carRepository;

    /* Go to url localhost:8080/alive to see output */
    @RequestMapping(value = "/alive")
    @ResponseBody
    public String alive() {
        return "car is alive";
    }

    @RequestMapping(value = "/cars")
    public String index(Model model) {
        model.addAttribute("cars", carRepository.findAll());
        return "index";
    }
}
