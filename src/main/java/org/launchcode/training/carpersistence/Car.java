package org.launchcode.training.carpersistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Car {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String make;
    private String model;

//    private int gasTankSize;
//    private double gasTankLevel;
//    private double milesPerGallon;
//    private double odometer = 0;


    public Car() {
    }

    public Car(String make, String model) {
        this.make = make;
        this.model = model;
    }


//    public Car(String make, String model, int gasTankSize, double milesPerGallon) {
//        this.make = make;
//        this.model = model;
//        this.gasTankSize = gasTankSize;
//        // Gas tank level defaults to a full tank
//        this.gasTankLevel = gasTankSize;
//        this.milesPerGallon = milesPerGallon;
//    }

    public int getId() {
        return id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

//
//    /**
//     * Drive the car an amount of miles. If not enough fuel, drive as far as fuel allows.
//     * Adjust fuel levels based on amount needed to drive the distance requested.
//     * Add miiles to odemeter.
//     *
//     * @param miles - the miles to drive
//     */
//    public void drive(double miles)
//    {
//        //adjust fuel based on mpg and miles requested to drive
//        double maxDistance = this.milesPerGallon * this.gasTankLevel;
//        double milesAbleToTravel = miles > maxDistance ? maxDistance : miles;
//        double gallonsUsed = milesAbleToTravel / this.milesPerGallon;
//        this.gasTankLevel = this.gasTankLevel - gallonsUsed;
//        this.odometer += milesAbleToTravel;
//    }
}
